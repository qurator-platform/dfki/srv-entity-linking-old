# srv-entity-linking

Service to retrieve URIs to recognised entities using wikidata.

(known) TODOs:
- add option for language (currently only working for English).
- currently taking first hit for a given label (assuming this is sorted by popularity). Look into smarter ways of getting a more relevant URI.
- only triggered by ('http://dbpedia.org/ontology/Person', 'http://dbpedia.org/ontology/Miscellaneous', 'http://dbpedia.org/ontology/Organisation', 'http://dbpedia.org/ontology/Location') entity types (). This depends on what the NER engine returns (currently only three of the four above). Will probably want to make this more lenient.
- dockerise

And generally largely untested.
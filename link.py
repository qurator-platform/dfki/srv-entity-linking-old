#!/usr/bin/python3

import requests
import sys
import nif
from collections import defaultdict

WIKIDATA_ROOTURL = 'https://en.wikipedia.org/'

def wikiLookup(inputstring, lang):

    url = 'https://query.wikidata.org/sparql'
    query = """

    SELECT distinct ?item ?itemLabel ?itemDescription WHERE{  
    ?item ?label "%s"@en.  
    ?article schema:about ?item .
    ?article schema:inLanguage "%s" .
    ?article schema:isPartOf <%s>. 
    SERVICE wikibase:label { bd:serviceParam wikibase:language "%s". }    
    }   
    """ % (inputstring, lang, WIKIDATA_ROOTURL, lang)
    
    r = requests.get(url, params = {'format': 'json', 'query': query})
    data = r.json()

    # for now, taking the first (assuming that this is the most popular) hit. Consider checking against type coming back from NER engine.
    wikiurl = None
    try:
        wikiurl = data['results']['bindings'][0]['item']['value']
    except (KeyError, IndexError) as e :
        sys.stderr.write('WARNING: URI for "%s" could not be retrieved (%s). Skipping...\n' % ( inputstring, e))
        
    return wikiurl


def addURIsToNIF(nifstring, informat, outformat, lang):

    n = nif.Nif()
    n.parseModelFromString(nifstring, informat)
    labels, uri2label = n.extractEntityLabels()

    ld = defaultdict(str)
    for label in labels:
        uri = wikiLookup(label, lang)
        ld[label] = uri
        
    
    for s, p, o in n.model.triples((None, n.ITSRDF.taClassRef, None)):
        if str(s) in uri2label:
            if ld[uri2label[str(s)]] != None:
                n.addURIToEntity(s, ld[uri2label[str(s)]])

    ser = n.model.serialize(format=outformat)
    return ser.decode('utf-8')

        

if __name__ == '__main__':
    
    #s = 'Trump'
    #lang = 'en'

    #wiki_url = wikiLookup(s, lang)
    #print(wiki_url)

    samplenif = """
@prefix itsrsdf: <http://www.w3.org/2005/11/its/rdf#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://qurator.ai/documents/1566481330.803265#char=0,130> a nif:Context,
        nif:String ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:endIndex "132"^^xsd:nonNegativeInteger ;
    nif:isString \"""
    Ich bin in Berlin, Julian ist in Madrid.   

    Kommt dir Julian Moreno Schneider bekannt   vor?
    Und was mit "Georg Rehm"?\""" .

<http://qurator.ai/documents/1566481330.803265#char=119,129> a nif:String ;
    nif:anchorOf "Georg Rehm" ;
    nif:beginIndex "120"^^xsd:nonNegativeInteger ;
    nif:endIndex "130"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Person" .

<http://qurator.ai/documents/1566481330.803265#char=16,22> a nif:String ;
    nif:anchorOf "Berlin" ;
    nif:beginIndex "16"^^xsd:nonNegativeInteger ;
    nif:endIndex "22"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Location" .

<http://qurator.ai/documents/1566481330.803265#char=38,44> a nif:String ;
    nif:anchorOf "Madrid" ;
    nif:beginIndex "38"^^xsd:nonNegativeInteger ;
    nif:endIndex "44"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Location" .

<http://qurator.ai/documents/1566481330.803265#char=64,87> a nif:String ;
    nif:anchorOf "Julian Moreno Schneider" ;
    nif:beginIndex "64"^^xsd:nonNegativeInteger ;
    nif:endIndex "87"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Person" .

    """

    n = addURIsToNIF(samplenif, 'turtle', 'turtle', 'en')
    print(n)
